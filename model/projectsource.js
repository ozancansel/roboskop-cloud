/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId;

var ProjectSource = new Schema({
    content         : { type : String , required : true },
    isFile          : { type : Boolean , required : true},
    commit          : { type : ObjectId , required : true , ref : 'ProjectCommit' },
    rPath           : { type : String , required : true}
});

ProjectSource.index({ commit : 1 , rPath  : 1  , commit  : 1 } , { unique : true });

module.exports = mongoose.model('ProjectSource', ProjectSource);

module.exports.ensureIndexes(function(err){
   if(err){
       console.log(err);
   }
});
