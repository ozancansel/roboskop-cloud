/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose        =   require('mongoose');
var co              =   require('co');
var ProjectSource   =   require('./projectsource');
var Schema          =   mongoose.Schema,
        ObjectId = Schema.ObjectId;

var ProjectCommitSchema = new Schema({
    message         : { type : String }   ,
    version         : { type : Number , required : true }   ,
    date            : { type : Date } ,
    project         : { type : ObjectId , required : true , ref : 'Project' } ,
    isCheckedSum    : { type : Boolean , required : true }
});

ProjectCommitSchema.index({ project  : 1 , version : 1 } , { unique : true });
ProjectCommitSchema.post('remove', function (doc) {
    co(function* (){

        var sources = yield ProjectSource.find({ commit : doc._id }).exec();
                
        for(var i = 0; i < sources.length; i++) {
            yield sources[i].remove();
        }
        
    });
});

module.exports = mongoose.model('ProjectCommit', ProjectCommitSchema);

module.exports.ensureIndexes(function(err){
   if(err){
       console.log(err);
   }
});