/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose        =   require('mongoose');
var co              =   require('co');
var ProjectCommit   =   require('./projectcommit');

var Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId;

var ProjectSchema = new Schema({
    name: {type: String, required: true},
    owner: {type: ObjectId, required: true, ref: 'User'}
});

ProjectSchema.index({name: 1, owner: 1}, {unique: true});
ProjectSchema.post('remove', function (doc) {
    co(function* (){

        var commits = yield ProjectCommit.find({ project : doc._id }).exec();
                
        for(var i = 0; i < commits.length; i++) {
            yield commits[i].remove();
        }
        
    });
});

module.exports = mongoose.model('Project', ProjectSchema);

module.exports.ensureIndexes(function (err) {
    if (err) {
        console.log(err);
    }
});

