/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;
var Project         =   require('./project');

var UserSchema =  new Schema({
    name        :   { type : String , required : true , index : { unique : true } },
    password    :   { type : String , required : true},
    admin       :   Boolean
});

UserSchema.post('remove', function (doc) {
    co(function* (){
        var projects = yield Project.find({ owner : doc._id }).exec();
                
        for(var i = 0; i < projects.length; i++) {
            yield projects[i].remove();
        }
    });
});
module.exports = mongoose.model('User' , UserSchema );

module.exports.ensureIndexes(function(err){
   if(err){
       console.log(err);
   }
});
