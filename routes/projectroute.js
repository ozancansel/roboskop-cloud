/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var express = require('express');
var router = express.Router();
var User = require('../model/user');
var Project = require('../model/project');
var ProjectSource = require('../model/projectsource');
var ProjectCommit = require('../model/projectcommit');
var co = require('co');
var jwt = require('jsonwebtoken');
var auth = require('../routeauth');


router.get('/newVersionNum', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;
        var projName = req.query.project;

        var project = yield Project.findOne({owner: user._id, name: projName});

        console.log(project);

        try {
            var lastCommit = yield ProjectCommit.findOne({project: project._id}).sort('-version').exec();
        } catch (err) {
            res.json({success: false, message: "Commit alınamadı."});
        }

        var version = 1;
        if (lastCommit !== null) {
            console.log("Not null")
            version = lastCommit.version + 1;
        }

        console.log(lastCommit);

        res.json({success: true, body: {version: version}});

    });

});

router.post('/commit', auth, function (req, res) {
    co(function* () {
        //<project>

        var proj = yield Project.findOne({name: req.body.project, owner: req.decoded._doc._id});
        var createdCommit;

        try {
            createdCommit = yield ProjectCommit.findOneAndUpdate({version: req.body.version, project: proj._id},
                    {$set: {"message": req.body.message, "date": new Date(), "isCheckedSum": false}},
                    {upsert: true , new : true});


        } catch (err) {
            console.log(err);
            res.json({success: false, message: "Yeni versiyon oluşturulurken hata oluştu."});

            return;
        }

        try {
                        
            console.log("_id -> " +  createdCommit._id);
            yield ProjectSource.findOneAndUpdate({  rPath: req.body.rPath, commit : createdCommit._id   },
                    {$set: {"content": req.body.content, "isFile": req.body.isFile}},
                    {upsert: true});

            res.json({success: true, message: "Başarıyla yazıldı."});

        } catch (err) {

            console.log("/commit --------------\n" + err);
            res.json({success: false, message: "Yazma işlemi sırasında hata oluştu."});

            return;
        }

    });
});

router.post('/endCommit', auth, function (req, res) {

    co(function* () {
        var proj = yield Project.findOne({name: req.body.project, owner: req.decoded._doc._id});

        try {
            var commit = yield ProjectCommit.findOneAndUpdate({version: req.body.version, project: proj._id},
                    {$set: {isCheckedSum: true}});

            console.log(commit);

            res.json({success: true, message: "İşlem başarılı"});
        } catch (err) {
            console.log(err);

            res.json({success: false, message: "Hata oluştu. "});
        }

    });

});

router.post('/upsertProject', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;

        try {

            var saved = yield Project.findOneAndUpdate({name: req.body.name, owner: user._id}, {
            }, {upsert: true, new : true}).exec();


            res.json({success: true, message: "Proje başarıyla oluşturuldu.", body: saved});

        } catch (err) {
            
            console.log("/upsertProject ------------------\n"  + err);
            if (err.code === 11000) {
                res.json({success: false, message: "Bu proje zaten mevcut."});
            } else {
                res.json({success: false, message: "Hata ! Proje oluşturulamadı."});
            }
        }

    }).catch(function (err) {
        res.json({success: false});
    });
});

router.get('/getProject', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;

        try {
            var projName = req.query.project;
            var commitV = req.query.version;

            var proj = yield Project.findOne({name: projName, owner: user._id});
            
            var projCommit;
            if (commitV !== undefined && commitV !== "-1")
                projCommit = yield ProjectCommit.findOne({version: commitV, project: proj._id});
            else
                projCommit = yield ProjectCommit.findOne({project: proj._id , isCheckedSum : true }).sort('-version').exec();

            var sources = yield ProjectSource.find({commit: projCommit._id}).exec();

            console.log(sources);
            res.json({success: true, body: {    files : sources }});

        } catch (err) {
            res.json({success: false, message: "Proje alınırken hata oluştu."});
        }
    });
});

module.exports = router;
