var express = require('express');
var router = express.Router();
var User = require('../model/user');
var Project = require('../model/project');
var ProjectSource = require('../model/projectsource');
var co = require('co');
var jwt = require('jsonwebtoken');
var auth = require('../routeauth');


router.post('/upsertProjectV2', auth, function (req, res) {

    co(function * () {

        var proj = yield Project.findOne({name: req.body.project});
        
        try{
        yield ProjectSource.findOneAndUpdate({rPath: req.body.rPath , project : proj._id },
                {$set: {"content": req.body.content, "isFile" : req.body.isFile}},
                {upsert: true});
              
            res.json({ success : true , message : "Başarıyla yazıldı."});
        }catch(err){
            res.json({ success : false , message : "Yazma işlemi sırasında hata oluştu."});
        }

    });
});

router.post('/authenticate', function (req, res) {
    User.findOne({name: req.body.name}, function (err, user) {

        if (err) {
            res.json('err');
            throw err;
        }


        if (!user) {
            res.json({success: false, message: "Authentication failed. User not found. "});
        } else if (user) {

            if (user.password !== req.body.password) {
                res.json({success: false, message: 'Authentication failed. Wrong password. '});
            } else {

                var token = jwt.sign(user, req.app.get('superSecret'), {
                    expiresIn: 1440
                });

                res.json({
                    success: true,
                    message: "Authentication accomplished.",
                    body: {
                        token: token
                    }
                });
            }
        }
    });

});

router.post('/createUser', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;

        if (!user.admin) {
            res.json({success: false, message: "Yetkisiz işlem."});
            return;
        }

        try {
            var saved = yield User.findOneAndUpdate({name: req.body.name},
                    {$set: {"name": req.body.name, "password": req.body.password, "admin": false}},
                    {upsert: true});

            res.json({success: true, message: "Kullanıcı başarıyla oluşturuldu/değiştirildi."});
        } catch (err) {
            console.log(err);
            res.json({success: false, message: "Hata ! Kullanıcı veritabanınıa yazılamıyor. "});
        }

    }).catch(function (err) {
        res.json({success: false, message: "Bilinmeyen bir hata oluştu."});
    });

});

router.post('/removeUser', auth, function (req, res) {

    co(function* () {

        var user = req.decoded._doc;

        if (!user.admin) {
            res.json({success: false, message: "Yetkisiz işlem."});

            return;
        }

        try {

            var result = yield User.findOne({name: req.body.name}).exec();
            yield result.remove();
            res.json({success: true, message: "Kullanıcı başarıyla silindi."});
        } catch (err) {
            console.log(err);
            res.json({success: false, message: "Silinme sırasında bilinmeyen bir hata oluştu."});
        }
    });

});


router.post('/upsertProject', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;
        console.log(req.body.name);

        try {
            var saved = yield Project.findOneAndUpdate({name: req.body.name, owner: user._id}, {
                $set: { "src": req.body.src }
            }, { upsert: true, new : true} ).exec();


            res.json({  success: true, message: "Proje başarıyla oluşturuldu.", data: saved });
            
        } catch (err) {
            
            if (err.code === 11000) {
                res.json({success: false, message: "Bu proje zaten mevcut."});
            } else {
                res.json({success: false, message: "Hata ! Proje oluşturulamadı."});
            }
        }

    }).catch(function (err) {
        res.json({success: false})
    });
});

router.get('/listProjects', auth, function (req, res) {
    co(function* () {
        var user = req.decoded._doc;

        var projects = yield Project.find({owner: user._id}).select('name').exec();

        res.json({success: true, body: {list: projects}});

    }).catch(function (err) {
        res.json({success: false, message: "Bilinmeyen bir hata oluştu !"})
    });
});

router.get('/getProjectOne', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;

        try {
            var project = yield Project.find({owner: user._id, name: req.query.name}).select('src').exec();

            res.json({success: true, message: "İşlem başarılı!", body: {src: project[0].src}});
        } catch (err) {
            console.log(err);

            res.json({success: false, message: "Bilinmeyen bir hata oluştu"});
        }
    }).catch(function (err) {
        console.log(err);
    });

});

router.get('/getProject' , auth , function(req,  res){
   
   co(function *(){
      
      var user = req.decoded._doc;
      
      try{
          var project = yield Project.find({ owner : user._id, name : req.query.name}).select('src').exec();
      }catch(err){
          
      }
       
   });
    
});

router.get('/checkToken', auth, function (req, res) {
    res.json({success: true});
});

router.post('/removeProject', auth, function (req, res) {

    co(function* () {
        var user = req.decoded._doc;
        try {

            console.log(req.body);
            var result = yield Project.findOne({name: req.body.name, owner: user._id}).exec();
            yield result.remove();


            res.json({success: true, message: "Proje başarıyla silindi."});

        } catch (err) {
            console.log(err);
            res.json({success: false, message: "Silinme işlemi sırasında bir hata oluştu. "});
        }

    }).catch(function (err) {
            res.json({success   :   false,  message :   "Hata oluştu."  });
    });

});

router.get('/', auth, function (req, res) {

    co(function* () {

        var results = yield User.find().exec();

        res.json({success: true, message: "", body: {users: results}});

    });
});

router.get('/setup', function (req, res) {


    co(function* () {

        try {
            var admin = new User({
                name: "admin",
                password: "123456",
                admin: true
            });
            var examples = new  User({
                name        :   "examples" ,
                password    :   "examples" ,
                admin       :   false
            });

            var result = [];

            result[0] = yield User.findOneAndUpdate({name : "admin"} ,
            { password : "123456" } , 
            { upsert : true , new : true , setDefaultsOnInsert : true } );
            result[1] = yield User.findOneAndUpdate(
            { name    :   "examples"} , 
            { password : "examples" } , 
            { upsert : true , new : true , setDefaultsOnInsert : true });
        } catch (err) {
            console.log(err);
        }

        res.json({success: false, data: result});
    }).catch(function (err) {
        res.json({success: false});
    });

});


module.exports = router;
